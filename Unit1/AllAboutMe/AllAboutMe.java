/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

/**
 *
 * @author user
 */
public class AllAboutMe {
    
    public static void main(String[] args) {
        
      String myName = "Clayton Gilbert";
      String myFavFood = "Pizza";
      int numOfPets = 7;
      boolean eatenGnocchi = false;
      int ageLearnedWhistle = 13;
      
        System.out.println( "My name is " + myName );
        System.out.println( "My favorite food is " + myFavFood );
        System.out.println( "The number of pets I own is " + numOfPets );
        System.out.println("Someone lied and said I have eaten Gnocchi! The rumors are absolutely " 
                + eatenGnocchi  );
        System.out.println("I learned to whistle at the young age of " + ageLearnedWhistle );
        
    }
    
    
}
