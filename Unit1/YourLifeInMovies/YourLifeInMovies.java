/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class YourLifeInMovies {
    
    public static void main(String[] args) {
        
        int userYearBorn;
        String userName;
         
        Scanner sc = new Scanner( System.in );
        
        System.out.println("What is your name? ");
        userName = sc.nextLine();
        
        System.out.println("What year was you born? ");
        userYearBorn = sc.nextInt();
        
        System.out.println("Well " + userName + "... ");
        
        if( userYearBorn < 2005 ){
            
            System.out.println("Pixar's Up came out a half a decade ago! ");
           
        }
        
        if( userYearBorn < 1995){
            
            System.out.println("Wow! The first harry potter movie came out 15 years ago!");
        
        }
        
        if( userYearBorn < 1985){
            
            System.out.println("Can you believe Space Jam came out NOT last decade, but the"
                    + " one BEFORE that!");
        }
            
        if( userYearBorn < 1975 ){
            
            System.out.println("The original Jurassic park is closer to the lunar landing than, than today");
            
        }
        
        if( userYearBorn < 1965){
            
            System.out.println("You do know MASH has been out half a century now right!");
            
        }
        
        
        
    }
    
    
    
    
}
