/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class Factorizer {

    public static void main(String[] args) {

        int userNumber;

        Scanner sc = new Scanner(System.in);

        System.out.println("Give me a number for FACTORIZER!!! (: ");
        userNumber = sc.nextInt();

        System.out.println("The factors of " + userNumber + " are: ");
        printFactors( userNumber );
        isPerfect( userNumber );
        isPrime( userNumber );

    }

    public static void printFactors( int number ) {

        for (int i = 1; i < number; i++) {

            if (number % i == 0) {

                System.out.printf("%d\n", i);

            }

        }

    }

    public static void isPerfect( int number ) {

        int temp = 0;

        for (int i = 1; i <= number / 2; i++) {

            if (number % i == 0) {
                temp += i;
            }
        }

        if (temp == number) {
            
            System.out.println(number + " is a perfect number");

        } else {
            
            System.out.println(number + " is not a perfect number");

        }
    }

    public static void isPrime( int number ) {

        for (int i = 2; i < number; i++) {

            if (number % i == 0) {

                System.out.println(number + " is not prime!");
                break;
                
            } else {

                System.out.println(number + " is prime!");
                break;

            }

        }
        
        System.out.println("Thank you for playing");

    }
    
    

}
