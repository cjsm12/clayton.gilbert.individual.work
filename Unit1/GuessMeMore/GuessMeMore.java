/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author user
 */
public class GuessMeMore {

    public static void main(String[] args) {

        Random rn = new Random();
        Scanner sc = new Scanner(System.in);
        int numberToGuess = rn.nextInt(100 + 1 + 100) - 100;
        int userGuess;
        boolean guessAgain = false;

        System.out.println("I am thinking of a number between -100 and 100...I bet you can't guess it?");       
        userGuess = sc.nextInt();
        
        guessAgain = true;

        while ( guessAgain ) {

            if (userGuess == numberToGuess) {

                System.out.println("Wow! That was it!");
                guessAgain = false;
            }

            if (userGuess < numberToGuess) {
                System.out.println("Your guess: " + userGuess);
                System.out.println("Ha too low! Try again!");
                userGuess = sc.nextInt();

            } else if (userGuess > numberToGuess) {

                System.out.println("Your guess: " + userGuess);
                System.out.println("Ha too high! Try again!");
                userGuess = sc.nextInt();
            }

        }

    }

}
