/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class BirthStones {
    
    public static void main(String[] args) {
        
        int monthChoice;
        
        Scanner sc = new Scanner( System.in );
        
        System.out.println("What month's birthstone are you wanting to know? Choose 1 - 12 ");
        monthChoice = sc.nextInt();
        
        if( monthChoice == 1 )
        {
            
             System.out.println( "January's birthstone is Garnet ");
            
        }else if( monthChoice == 2 )
        {
           
             System.out.println("February's birthstone is Amethyst ");
            
        }else if( monthChoice == 3 )
        {
            
             System.out.println("March's birthstone is Aquamarine ");
            
        }else if( monthChoice == 4 )
        {
            
            System.out.println("April's birthstone is Diamond ");
            
        }else if( monthChoice == 5)
        {
            
            System.out.println("May's birthstone is Emerald ");
            
        }else if( monthChoice == 6)
        {
            
            System.out.println("June's birthstone is Pearl ");
            
        }else if( monthChoice == 7 )
        {
            
            System.out.println("July's birthstone is Ruby ");
            
        }else if( monthChoice == 8 )
        {
            
            System.out.println("August's birthstone is Peridot ");
            
        }else if( monthChoice == 9 )
        {
            
            System.out.println("September's birthstone is Sapphire "); 
            
        }else if( monthChoice == 10 )
        {
            
            System.out.println("October's birthstone is Opal ");
            
        }else if ( monthChoice == 11 )
        {
            
            System.out.println("November's birthstone is Topaz ");
            
        }else if( monthChoice == 12 )
        {
            
            System.out.println("December's birthstone is Turquoise ");
            
        }else
            System.out.println("Error, please input a number between 1 - 12");
        
        
        
    }
    
    
    
    
}
