/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

import java.util.Random;

/**
 *
 * @author user
 */
public class FortuneCookie {
    
    
    public static void main(String[] args) {
        
        Random rn = new Random();
        
        int choice = rn.nextInt( 11 );
        
        if( choice == 1 ){
            
            System.out.println("Your Geek fortune: Try not. Do or do not. There is no try "); 
            
        }else if( choice == 2 ){
           
            System.out.println("Your Geek fortune: Never argue with the data.");
            
        }else if( choice == 3 ){
           
            System.out.println("Your Geek fortune: With great power comes - great responsibility.");
 
        }else if( choice == 4 ){
            
            System.out.println("Your Geek fortune: Goonies never say die");
            
        }else if( choice == 5 ){
            
            System.out.println("Your Geek fortune: Never go in against a Sicilian when death is on the line! ");
            
        }else if( choice == 6 ){
            
            System.out.println("Your Geek fortune: Those aren't the droids you're looking for");
            
        }else if( choice == 7 ){
            
            System.out.println("Your Geek fortune: You are a leaf on the wind, watch how you soar");
            
        }else if( choice == 8 ){
            
            System.out.println("Your Geek fortune: Do absolutely nothing, and it will be everything you thought it could be.");
            
        }else if ( choice == 9 ){
            
            System.out.println("Your Geek fortune: Kneel before Zod.");
            
        }else if( choice == 10 ){
            
            System.out.println("Your Geek fortune: Make it so");
            
        }
    }
    
}
