/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

import java.util.Random;

/**
 *
 * @author user
 */
public class CoinFlipper {
    
    public static void main(String[] args) {
        
        Random rn = new Random();
        
        System.out.println("Ready, Set, Flip...");
        
        boolean coin = rn.nextBoolean();
        
        if( coin ){
            
            System.out.println("You got HEADS!");
        }else
            
            System.out.println("You got TAILS!");
        
        
        
        
    }
    
    
    
}
