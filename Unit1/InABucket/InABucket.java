/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

/**
 *
 * @author user
 */
public class InABucket {
    
    public static void main(String[] args) {
        
      //Declaration
      String walrus;
      double piesEaten;
      float weightOfTeacupPig;
      int grainsOfSand;
      
      //BUT declaring them just sets up the space for the variables
      //To use the variable, you have to put data IN them first
      
      //Initialization
      walrus = "Sir Leroy Jenkins III";
      piesEaten = 42.1;
      weightOfTeacupPig = 13.05f; 
      grainsOfSand = 1000000000;
      
        System.out.println( "Meet my pet walrus " + walrus );
        System.out.print( "He was a bit hungry today, and ate this many pies : " );
        System.out.println( piesEaten );
        
        System.out.println( "The weight of my teacup pig is very small at just around " + 
                weightOfTeacupPig + " ounces heavy!. Isn't he cute? " );
        
        System.out.println( "There is alot of sand covering the beaches of this earth! " );
        System.out.print( "Would you believe me if I told you there were " );
        System.out.println( grainsOfSand + " pieces of grains of sand covering those beaches? " );
     
        
        
        
    }
    
    
    
    
    
}
