/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class TriviaNight {

    public static void main(String[] args) {

        int userChoice;
        int correctCounter = 0;
        int wrongCounter = 0;
        boolean play = false;

        Scanner sc = new Scanner(System.in);

        System.out.println("Welcome to TriviaPursuit! ");

        System.out.println("Question #1: What is the best programming langueage? \n");
        System.out.printf("%-10s", "1) Java");
        System.out.printf("%20s", "2) Javascript\n");
        System.out.printf("%-10s", "3) C++");
        System.out.printf("%11s", "4) C#\n");
        userChoice = sc.nextInt();

        if (userChoice == 1) {

            System.out.println("YOUR CHOICE " + userChoice);
            correctCounter++;

        } else {
            System.out.println("YOUR CHOICE " + userChoice);
        }
        wrongCounter++;

        System.out.println("\nQuestion #2: Who is the lead actor in Lethal Weapon? \n");
        System.out.printf("%-10s", "1) Chuck Norris");
        System.out.printf("%20s", "2) Bruce Lee\n");
        System.out.printf("%-10s", "3) Mel Gibson");
        System.out.printf("%20s", "4) Bon Jovi\n");
        userChoice = sc.nextInt();

        if (userChoice == 3) {

            System.out.println("YOUR CHOICE " + userChoice);
            correctCounter++;

        } else {
            System.out.println("YOUR CHOICE " + userChoice);
        }
        wrongCounter++;

        

        do {

            System.out.println("\nQuestion #3: Which video game series is about a treasure hunter named Drake? \n");
            System.out.printf("%-10s", "1) Resident Evil");
            System.out.printf("%20s", "2) Metal Gear\n");
            System.out.printf("%-10s", "3) Silent Hill");
            System.out.printf("%20s", "4) Uncharted\n");
            userChoice = sc.nextInt();

            //Trying with switch statments, Best method to me due to the easily added validation check...
            switch (userChoice) {

                case 1:
                case 2:
                case 3:
                    System.out.println("YOUR CHOICE " + userChoice);
                    wrongCounter++;
                    play = false;
                    break;
                case 4:
                    System.out.println(" Your Choice " + userChoice);
                    correctCounter++;
                    play = false;
                    break;
                default:
                    System.out.println("Please input a valid selection 1 - 4");
                    play = true;

            }

        } while ( play );

        System.out.println("\nThank you for playing\n");
        System.out.println("You Got " + correctCounter + " right!");

    }

}
