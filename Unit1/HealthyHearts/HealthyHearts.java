/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class HealthyHearts {
    
    public static void main(String[] args) {
        
        int userAge;
        int maximumHeartRate;
        int minTargetHeartRate;
        int maxTargetHeartRate;
        
        Scanner sc = new Scanner( System.in );
        
        System.out.println("What is your age? ");
        userAge = sc.nextInt();
        
        //Setting variables using methods
        maximumHeartRate = calculateMaxHeartRate( userAge );
        minTargetHeartRate = minHeartRate( maximumHeartRate );
        maxTargetHeartRate = maxHeartRate( maximumHeartRate );
        
        System.out.println("Your maximum heart rate for your age should be " + maximumHeartRate );
        
        //These are what would need to be done if methods were not used...hard coded version
        //minTargetHeartRate = (int) (maximumHeartRate * .50);
        //maxTargetHeartRate = (int) (maximumHeartRate * .85);
        
        
        System.out.println("Your target HR Zone is " + minTargetHeartRate + " - " + maxTargetHeartRate + 
                " beats per minute.");
         
        
    }
    
    public static int calculateMaxHeartRate( int heartRate ){
        
        return 220 - heartRate;
        
    }
    
    public static int minHeartRate(int heartRate ){
        
        return (int) ( heartRate * .50 );
        
    }
    
    public static int maxHeartRate( int heartRate ){
        
        return (int) ( heartRate * .85 );
        
    }
    
    
    
}
