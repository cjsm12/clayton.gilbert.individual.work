/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

import java.util.Random;

/**
 *
 * @author user
 */
public class Opinionator {
    
    public static void main(String[] args) {
        
        Random rn = new Random();
        
        System.out.println("I can't decide what animal I like best? ");
        System.out.println("I know, I'll have ransdom decide for me!");
        
        int animal = rn.nextInt( 6 );
        
        if( animal == 0 ){
            
            System.out.println("LLamas");
            
        }else if( animal == 1 ){
            
            System.out.println("Dodos");
            
        }else if( animal == 2 ){
            
            System.out.println("Wooly Mammoths");
            
        }else if( animal == 3 ){
            
            System.out.println("Sharks");
            
        }else if( animal == 4 ){
            
            System.out.println("Cockatoos");
            
        }else if( animal == 5 ){
            
            System.out.println("Mole Rat");
            
        }
        
        System.out.println("Thanks random, maybe you are the best!");
            
        //2) The bug was the fact you were only generating a number between 0 and 4, which is only 5 choices
        //and you have 6 choices to choose from (0 - 5), so in order to generate a random number between
        //0 and 5 you need a 6 as the argument instead of a 5
    }
    
    
}
