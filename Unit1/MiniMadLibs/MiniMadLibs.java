/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class MiniMadLibs {
    
    
    public static void main(String[] args) {
        
        String noun;
        String adjective;
        String noun2;
        int number;
        String adjective2;
        String pluralNoun, pluralNoun2, pluralNoun3;
        String verbPresentTense;
        String verbPastTense;
        
        Scanner sc = new Scanner( System.in );
        
        System.out.println("Welcome to the MadLIBS game! ");
        
        System.out.print("1) Noun: ");
        noun = sc.next();
        System.out.print("2) Adj: ");
        adjective = sc.next();
        System.out.print("3) Another noun: ");
        noun2 = sc.next();
        System.out.print("4) Give me a number: ");
        number = sc.nextInt();
        System.out.print("5) Another adjective: ");
        adjective2 = sc.next();
        System.out.print("6) One plural noun ");
        pluralNoun = sc.next();
        System.out.print("7) Another plural noun ");
        pluralNoun2 = sc.next();
        System.out.print("8) One last plural noun please: ");
        pluralNoun3 = sc.next();
        System.out.print("9) A verb in the present tense: ");
        verbPresentTense = sc.next();
        System.out.print("10) A verb but in the past tense: \n");
        verbPastTense = sc.next();
        
        
        System.out.println("******Now Lets get Mad (LIBS)******** ");
        
        System.out.println(noun + ": the " + adjective + " frontier. These are the voyages of the starship " + noun2 + ". Its " + number + "-year mission: to explore\n" +
"strange " + adjective2 + pluralNoun + ", to seek out " + adjective2 + pluralNoun2 + " and " + adjective2 + pluralNoun3 + ", to boldly " + verbPresentTense + " where no one has " + verbPastTense + " before.");
        
        
        
        
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
}
