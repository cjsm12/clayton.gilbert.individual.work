/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author user
 */
public class RockPaperScissorsThree {

    public static void main(String[] args) {

        
        int userChoice;
        int computerChoice;
        int roundsToPlay;
        int roundCount = 0;
        boolean keepPlaying = true;
        int win = 0;
        int lose = 0;
        int tie = 0;
        int compWin = 0;
        int compLose = 0;
        int compTie = 0;

        Scanner sc = new Scanner(System.in);
        Random rn = new Random();

        System.out.println("Welcome to Rock/Paper/Scissors V3.0!!!\n");
        System.out.println("How many rounds do you want to play today?");
        roundsToPlay = sc.nextInt();

        
        if (roundsToPlay > 1 && roundsToPlay <= 10) {

            do {

                System.out.println("Choose a weapon: 1)Rock 2)Paper 3)Scissors ");
                userChoice = sc.nextInt();

                computerChoice = rn.nextInt(3) + 1;

                if (userChoice == computerChoice) {

                    System.out.println("Tie Sucka!");
                    roundCount++;
                    tie++;
                    compTie++;

                } else if (userChoice == 1 && computerChoice == 2) {

                    System.out.println("Darn you Lose! Paper swallows Rock!");
                    roundCount++;
                    lose++;
                    compWin++;

                } else if (userChoice == 1 && computerChoice == 3) {

                    System.out.println("Nice you win! Rock crushes Scissors!");
                    roundCount++;
                    win++;
                    compLose++;

                } else if (userChoice == 2 && computerChoice == 1) {

                    System.out.println("Nice you win! Paper swallows Rock!");
                    roundCount++;
                    win++;
                    compLose++;

                } else if (userChoice == 2 && computerChoice == 3) {

                    System.out.println("Darn you lose! Scissors slice da Paper!");
                    roundCount++;
                    lose++;
                    compWin++;

                } else if (userChoice == 3 && computerChoice == 1) {

                    System.out.println("Darn you lose! Rock crushes Scissors!");
                    roundCount++;
                    lose++;
                    compWin++;

                } else if (userChoice == 3 && computerChoice == 2) {

                    System.out.println("Nice you win! Scissors slice da paper!");
                    roundCount++;
                    win++;
                    compLose++;

                } else {

                    System.out.println("Please choose a value between 1 and 3");

                }

                //Checker
                if (roundCount == roundsToPlay) {

                    keepPlaying = false;

                }

            } while (keepPlaying);

        } else {

            System.out.println("Choose a valid round count between 1 and 10 next time please!");
        }

        //Results
        System.out.println("Results: \n");
        System.out.println("You won " + win + " rounds, lost " + lose + " rounds, and tied " + tie + " times!");
        System.out.println("The computer won " + compWin + " rounds, lost " + compLose + " rounds and tied " + compTie + " times!\n");

        if (win > compWin) {

            System.out.println("You WIN! You beat the computer " + win + " times out of " + roundCount + " rounds with " + tie + " ties!");

        } else if (win == compWin) {

            System.out.println("You both TIED!");

        } else {
            System.out.println("You LOST! The computer won " + compWin + " times out of " + roundCount + " rounds with " + compTie + " ties!");
        }

    }

}
