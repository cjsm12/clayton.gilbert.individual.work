/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class FieldDay {

    //If first character of last name is greater than the name being compared the value will be positive
    //Greater meaning coming later in the alphabet
    //Lesser meaning coming earlier than the alphabet and result will be -number
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("What's you last name? ");
        String lastName = sc.nextLine();

        if( lastName.compareTo( "Baggins" ) < 0 ){
            
            System.out.println( "You're on team Red Dragons!" );
            
        }else if( lastName.compareTo( "Baggins" ) > 0 && lastName.compareTo( "Dresden" ) < 0 ){
            
            System.out.println( "You're on team Dark Wizards!" );
            
        }else if( lastName.compareTo( "Dresden" ) > 0 && lastName.compareTo( "Howl" ) < 0 ){
            
            System.out.println( "You're on team Moving Castles!" );
            
        }else if( lastName.compareTo( "Howl" ) > 0 && lastName.compareTo( "Potter" ) < 0 ){
            
            System.out.println( "Your're on team Golden Snitches!" );
            
        }else if( lastName.compareTo( "Potter" ) > 0 && lastName.compareTo( "Vimes" ) < 0 ){
            
            System.out.println( "You're on team Night Guards!" );
            
        }else if( lastName.compareTo( "Vimes" ) > 0 ){
            
            System.out.println( "You're on team Black Holes!" );
        }
     
        
    }

}
