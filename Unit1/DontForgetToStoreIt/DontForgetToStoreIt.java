/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class DontForgetToStoreIt {
    
    public static void main(String[] args) {
        
     Scanner sc = new Scanner(System.in);
     
     int meaningOfLifeAndEverything = 42;
     double pi = 3.14159;
     String cheese, color;
     
        System.out.println("Give me pi to at least 5 decimals ");
        pi = sc.nextDouble();
        
        System.out.println("What is the meaning of life, the universe, and everything? ");
        meaningOfLifeAndEverything = sc.nextInt();
        
        System.out.println("What is your favorite kind of cheese? ");
        cheese = sc.next();
        
        System.out.println("Do you like the color red or blue better? ");
        color = sc.next();
        
        System.out.println("Ooh, " + color + " " + cheese + " sounds delicious!");
        System.out.println("The circumference of life is " +(2 * pi * meaningOfLifeAndEverything) );
        
        
    }
    
    
}
