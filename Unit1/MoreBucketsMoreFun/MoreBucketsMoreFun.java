/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

/**
 *
 * @author user
 */
public class MoreBucketsMoreFun {
    
    public static void main(String[] args) {
        
        //Declaration
        int butterflies, bugs, beetles;
        String color, size, shape, thing;
        double number;
        
        //Initialization
        butterflies = 2;
        beetles = 4;
        bugs = butterflies + beetles;
        
        System.out.println( "There are only " + butterflies + " butterflies," );
        System.out.println("but " + bugs + " bugs total.");
        
        System.out.println("Uh oh! My dog ate one! ");
        butterflies--;
        //bugs--;This is what should have been added to keep track of the total of bugs
        System.out.println("Now there are only " + butterflies + " butterflies left");
        System.out.println("But still " + bugs + " bugs left...Wait a minute!");
        System.out.println("Maybe I just counted wrong the first time.");
        
        
        //Questions:
        //2)Decrement operator on butterfly
        
        //3)Because we decremented the butterflies variable only which is a variable all on its own with its own space in memory,
        //If we wanted to keep track of the total amount of bugs after the dog ate a butterfly, we should
        //have instead decremented the bugs variable instead, or actually both the butterfly and the bugs
        //since we are keeping track of both.
        
        
        
        
        
        
    }
    
    
    
}
