/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class PickyEater {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("How many times has it been fried? # ");
        int timesFried = sc.nextInt();

        System.out.print("Does it have any spinach in it? y/n ");
        String anySpinach = sc.next();

        System.out.print("Is it covered in cheese? y/n ");
        String cheeseCovered = sc.next();

        System.out.print("How many pats or butter are on top? # ");
        int butterOnTop = sc.nextInt();

        System.out.print("Is it covered in chocolate? y/n ");
        String chocolateCovered = sc.next();

        System.out.print("Does it have a funny name? y/n ");
        String funnyName = sc.next();

        System.out.print("Is it broccoli? y/n ");
        String isBroccoli = sc.next();
        

        if (anySpinach.equals("y") || funnyName.equals("y")) {

            System.out.println("There is no way that'll get eaten!");

        } else if ( isBetween(timesFried, 2, 4) && chocolateCovered.equals("y")) {

            System.out.println("Oh, it's like a deep fried snickers! Yummy");

        } else if (timesFried == 2 && cheeseCovered.equals("y")) {

            System.out.println("Mmm. Yeah, fried cheesey doodles will get eat.");

        } else if (isBroccoli.equals("y") && butterOnTop > 6) {

            //nested if route for three plus condition checks
            if (cheeseCovered.equals("y")) {
                
                System.out.println("As long as the green is hidden by cheddar, it'll happen!");
                
            }

        } else if ( isBroccoli.equals("y") ) {
            
            System.out.println("Oh, green stuff like that can go in the bin.");
            
        }
    }
    
    //Tried method route instead of nested if statement
    public static boolean isBetween(int value, int min, int max) {

        return ((value > min) && (value < max));

    }

}
