/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class InterestCalculator {
    
    public static void main(String[] args) {
        
        int initialAmount;
        double rate;
        double rateAsDecimal;
        double compoundsYear = 4;
        int years;
        double newPrinciple;
        double interestEarned;
        
        Scanner sc = new Scanner( System.in );
        
        
        System.out.println("Welcome to the interest calculator!\n");
        
        System.out.println("What is the initial amount you want to invest? ");
        initialAmount = sc.nextInt();
        
        System.out.println("What is the annual interest rate? ");
        rate = sc.nextDouble();
        rateAsDecimal = rate / 100;
        
        System.out.println("Lastly, how long do you want the money to stay in the fund? ");
        years = sc.nextInt();
        
        
            
            for( int i = 1; i < years + 1; i++ ){
            
                
            interestEarned = Math.pow( 1 + ( rateAsDecimal/compoundsYear ), compoundsYear * i);    
                
            System.out.printf("Interest earned: %.2f ", interestEarned );
            
            newPrinciple = initialAmount * Math.pow( 1 + ( rateAsDecimal/compoundsYear ), compoundsYear * i);
                        
            System.out.printf( "Year " + i + ": The original principle of $%d at the beginning of the year is now $%.2f at the end of year %d\n", initialAmount, newPrinciple, i );
            
            
        }
            
    
            
        
        
        
        
    }
    
   
    
    
}
