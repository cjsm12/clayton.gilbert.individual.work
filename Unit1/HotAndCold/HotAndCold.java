/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author user
 */
public class HotAndCold {

    public static void main(String[] args) {

        Random rn = new Random();
        Scanner sc = new Scanner(System.in);

        int randomNumber = rn.nextInt(1001) + 1;

        System.out.println("I have picked a number between 1 and 1000! You think you can guess it?");
        int userPick = sc.nextInt();
        int finalNumber = randomNumber - userPick;

        System.out.println("You chose " + userPick + "\n");

        if (finalNumber == randomNumber) {

            System.out.println("What are you a mind reader? YOU GOT IT!");

        } else if (finalNumber > 500) {

            System.out.println("So cold, you're practically in the north pole!");
            System.out.println("I ACTUALLY chose " + randomNumber);

        } else if (finalNumber < 500 && finalNumber > 201) {

            System.out.println("Pretty chilli!");
            System.out.println("I ACTUALLY chose " + randomNumber);

        } else if (finalNumber < 200 && finalNumber > 101) {

            System.out.println("Tepid, but approaching room temp!");
            System.out.println("I ACTUALLY chose " + randomNumber);

        } else if (finalNumber < 100 && finalNumber > 51) {

            System.out.println("Warm, but still a bit off");
            System.out.println("I ACTUALLY chose " + randomNumber);

        } else if (finalNumber < 50 && finalNumber > 21) {

            System.out.println("Warming up...");
            System.out.println("I ACTUALLY chose " + randomNumber);

        } else if (finalNumber < 20 && finalNumber > 10) {
            
            System.out.println("Hot hot hot, you're practically bubbling!");
            System.out.println("I ACTUALLY chose " + randomNumber);

        } else if (finalNumber < 9 && finalNumber > 1) {

            System.out.println("So hot, it's like you're living on the SUN!");
            System.out.println("I ACTUALLY chose " + randomNumber);

        }

    }

}
