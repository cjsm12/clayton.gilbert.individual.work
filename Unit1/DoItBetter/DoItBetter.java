/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class DoItBetter {
    
    public static void main(String[] args) {
        
        int milesRun;
        int hotdogsEaten;
        int languagesKnown;
        
        Scanner sc = new Scanner( System.in );
        
        
        System.out.println("How many miles can you run at one time? ");
        milesRun = sc.nextInt();
        System.out.println(milesRun + "? Amateurish! I can do " + ICanDoBetter(milesRun));
        
        System.out.println("How many hotdogs can you eat in one setting? ");
        hotdogsEaten = sc.nextInt();
        System.out.println(hotdogsEaten + "? Pff! That's not going to cut it in this tournament! You "
                + "need to eat at least as much as me at " + ICanDoBetter(hotdogsEaten) + " in order to win!");
        
        System.out.println("Maybe you know more languages than me? Try me...");
        languagesKnown = sc.nextInt();
        
        System.out.println("Haha! Only " + languagesKnown + "? Can you believe I know " + ICanDoBetter(languagesKnown)
        + " languages?");
        
        
        
        
        
        
        
    }
    
    public static int ICanDoBetter( int number ){
        
        return ( number * 2 ) + 1;
        
    }
    
    
    
}
