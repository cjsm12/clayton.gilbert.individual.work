/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

/**
 *
 * @author user
 */
public class TheOrderOfThings {
    
    public static void main(String[] args) {
        
        double number;
        String opinion, size, age, shape, color, origin, material, purpose;
        String noun;
        
        number = 5.0;
        opinion = "AWESOME";
        size = "teensy-weensy";
        age = "new";
        shape = "oblong";
        color = "BRIGHT yellow";
        origin = "AlphaCentaurian";
        material = "platinum";
        purpose = "good";
        
        noun = "dragons";
        
        //Using the + doesn't add, it concatonates or adds them together
        
        //This way sounds the best!
        System.out.println( "The old " + color + " " + shape + " " + noun + " are " + purpose + " but " + size + " ,however the " + age + " and improved "
                + number + " " + opinion + " " + origin + " " + material + " " + noun + " are much better!" );
        
        
    }
    
    
    
}
