/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swgunit1;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class KnockKnock {
    
    public static void main(String[] args) {
        
        String correctAnswer = "Marty McFly";
        String userGuess;
        
        
        Scanner sc = new Scanner( System.in );
        
        
        System.out.print("Knock Knock! Guess who? ");
        userGuess = sc.nextLine();
        
        if( userGuess.equalsIgnoreCase(correctAnswer) ){
            
            System.out.println("Hey! That's right! I'm back!");
            System.out.println("... From the future.");

        }else
            System.out.println("Dude, do I look like " + userGuess + "?");
      
        
        //2) Using the == doesn't work because since String is an object and not a primitive data type
        //like an integer or double is, when comparing you need to use the .equals operator which
        //compares two objects
        
        //3) If you don't type in the right capitilization, the program will run into the else statement
        //becuase even though its the same name, it's not EXACTLY like the way it's supposed to be typed
        //however if you use a method that is part of the String class called equalsIgnoreCase and input
        //the string you are comparing as the argument, it will compare your guess with the right answer
        //regardless of capitilization! I implemented this method above
        
        
    }
    
    
    
    
}
